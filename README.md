# Learning Where to Look

Recent success of Joint Embedding (JE) methods shows, that pairwise relationships are enough to learn powerful representations. This is achieved by differentially augmenting an image (e.g.: crop, rotate, noise) and maximizing the agreement of the generated representations. In medical images we are often interested in a particular structure in the image (e.g.: lesion, tumor, metastasis). The learned representations, however, are not guaranteed to compress the information of the structure of interest. We present a framework to control what structures contribute to the image representation, i.e., where to look. This is achieved by a simple neural network that suggests crops, that are fed into the JE method.

#### Original image for poster
![original_image](./original_image.png)


## Crop Suggestion Network
![csn](./csn.png)

## The International Skin Imaging Collaboration​ (ISIC) dataset
Dataset: [here](https://www.isic-archive.com/)
![isic_example](./isic/image_01.png)
Find more examples of the CSN here: [./isic](./isic/)


## The PASCAL Visual Object Classes (VOC) dataset
Dataset: [here](http://host.robots.ox.ac.uk/pascal/VOC/)
![voc_example](./voc/image_14.png)
Find more examples of the CSN here: [./voc](./voc/)
